﻿namespace Pepper.Models
{
    internal class AccountType
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
