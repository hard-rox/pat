﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Pepper.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TranscationsPage : ContentPage
    {
        public TranscationsPage()
        {
            InitializeComponent();

            transactionsListView.ItemsSource = new List<string>() { "jehe", "ww", "ee", "ee" };
        }

        private void TransactionsListView_Refreshing(object sender, EventArgs e)
        {
            transactionsListView.EndRefresh();
        }
    }
}