﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Pepper.Components
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TransactionCell : ViewCell
    {
        public TransactionCell()
        {
            InitializeComponent();
        }
    }
}